'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var csscomb = require('gulp-csscomb');

gulp.task('sass', function () {
  return gulp.src('src/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('.tmp/css'));
});

gulp.task('buildcss', ['sass'], function() {
  return gulp.src('.tmp/css/main.css')
    .pipe(csscomb())
    .pipe(gulp.dest('src/css'));
});

gulp.task('watch', function () {
  gulp.watch('src/scss/**/*.scss', ['buildcss']);
});

gulp.task('default', ['buildcss', 'watch']);