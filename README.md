# SCSS Assignment

Construct a SCSS file to output the following CSS styles.


```css
.faq ul,
nav ul {
  list-style: none;
  margin: 0;
  padding: 0;
}

html {
  font-family: "Open Sans", sans-serif;
  font-size: 16px;
}

h1 {
  color: #333333;
  font-family: "Roboto", sans-serif;
  font-size: 3em;
  font-weight: 400;
  line-height: 1em;
  margin-bottom: 10px;
}

h2 {
  color: #333333;
  font-family: "Roboto", sans-serif;
  font-size: 2.25em;
  font-weight: 400;
  line-height: 1em;
  margin-bottom: 10px;
}

h3 {
  color: #333333;
  font-family: "Roboto", sans-serif;
  font-size: 2em;
  font-weight: 300;
  line-height: 1em;
  margin-bottom: 10px;
}

p {
  color: #333333;
  font-family: "Open Sans", sans-serif;
  font-size: 1em;
  font-weight: 300;
  line-height: 1em;
  margin-bottom: 10px;
}

.btn {
  border: none;
  cursor: pointer;
  display: inline-block;
  font-family: "Roboto", sans-serif;
  font-size: 1em;
  font-weight: 400;
  padding: 8px 22px;
  text-align: center;
  transition: background-color 200ms ease-in-out, color 200ms ease-in-out, opacity 200ms ease-in-out;
}
.btn .btn-primary {
  background-color: #D75893;
  color: #FFFFFF;
}
.btn .btn-primary:hover {
  background-color: #CB3179;
}
.btn .btn-primary--inverted {
  background-color: #FFFFFF;
  color: #D75893;
}
.btn .btn-primary--inverted:hover {
  background-color: #FFFFFF !important;
  color: #CB3179;
}

.faq {
  padding: 50px 0;
}
.faq li {
  border-bottom: 1px solid #333333;
  padding: 15px 0 15px 15px;
  position: relative;
}

nav li {
  float: left;
  font-family: "Roboto", sans-serif;
  font-size: 1em;
  font-weight: 500;
  padding: 6px 10px;
}
```

##  Installation & Run

 1. Install [Yarn](https://yarnpkg.com/en/docs/install) to your local environment.
 2. Run `yarn` to install the dependencies.
 3. Run `yarn start` to start the dev server. CSS output will be automatically generated on every write to the `main.css` file.
